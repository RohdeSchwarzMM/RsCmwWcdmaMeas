UeChannels
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UECHannels:BSFSelection

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UECHannels:BSFSelection



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels.UeChannels
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueChannels.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeChannels_Carrier.rst