Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPCch:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPCch:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPCch:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPCch:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Edpcch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: