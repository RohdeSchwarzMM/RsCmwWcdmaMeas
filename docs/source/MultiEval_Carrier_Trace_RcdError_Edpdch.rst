Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.carrier.trace.rcdError.edpdch.repcap_edpdChannel_get()
	driver.multiEval.carrier.trace.rcdError.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Edpdch.Edpdch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.edpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Edpdch_Current.rst
	MultiEval_Carrier_Trace_RcdError_Edpdch_Average.rst
	MultiEval_Carrier_Trace_RcdError_Edpdch_Maximum.rst
	MultiEval_Carrier_Trace_RcdError_Edpdch_StandardDev.rst