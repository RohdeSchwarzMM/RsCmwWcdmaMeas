Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfLeft_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: