State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:OLPControl:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:OLPControl:STATe



.. autoclass:: RsCmwWcdmaMeas.Implementations.OlpControl_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.olpControl.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	OlpControl_State_All.rst