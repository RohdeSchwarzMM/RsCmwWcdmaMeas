StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPCCh:SDEViation
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPCCh:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPCCh:SDEViation
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPCCh:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdPower_.Dpcch_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: