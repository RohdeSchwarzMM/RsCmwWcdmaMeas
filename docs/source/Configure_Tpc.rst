Tpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:CSELection
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:SETup
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:MODE
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:MOEXception
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:TOUT

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:CSELection
	CONFigure:WCDMa:MEASurement<Instance>:TPC:SETup
	CONFigure:WCDMa:MEASurement<Instance>:TPC:MODE
	CONFigure:WCDMa:MEASurement<Instance>:TPC:MOEXception
	CONFigure:WCDMa:MEASurement<Instance>:TPC:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc.Tpc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tpc_IlpControl.rst
	Configure_Tpc_Monitor.rst
	Configure_Tpc_Mpedch.rst
	Configure_Tpc_Ctfc.rst
	Configure_Tpc_Ulcm.rst
	Configure_Tpc_Dhib.rst
	Configure_Tpc_Limit.rst