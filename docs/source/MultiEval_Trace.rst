Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Phd.rst
	MultiEval_Trace_CdeMonitor.rst
	MultiEval_Trace_CdpMonitor.rst
	MultiEval_Trace_Carrier.rst
	MultiEval_Trace_Emask.rst
	MultiEval_Trace_EvMagnitude.rst
	MultiEval_Trace_Merror.rst
	MultiEval_Trace_Perror.rst
	MultiEval_Trace_Iq.rst