StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDERror:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDERror:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.CdError_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: