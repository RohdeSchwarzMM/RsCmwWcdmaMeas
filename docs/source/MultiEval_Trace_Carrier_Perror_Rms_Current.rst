Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CARRier<Carrier>:PERRor:RMS:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CARRier<Carrier>:PERRor:RMS:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Carrier_.Perror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: