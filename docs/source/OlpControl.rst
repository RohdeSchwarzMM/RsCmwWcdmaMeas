OlpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:MEASurement<Instance>:OLPControl
	single: ABORt:WCDMa:MEASurement<Instance>:OLPControl
	single: INITiate:WCDMa:MEASurement<Instance>:OLPControl
	single: READ:WCDMa:MEASurement<Instance>:OLPControl
	single: FETCh:WCDMa:MEASurement<Instance>:OLPControl
	single: CALCulate:WCDMa:MEASurement<Instance>:OLPControl

.. code-block:: python

	STOP:WCDMa:MEASurement<Instance>:OLPControl
	ABORt:WCDMa:MEASurement<Instance>:OLPControl
	INITiate:WCDMa:MEASurement<Instance>:OLPControl
	READ:WCDMa:MEASurement<Instance>:OLPControl
	FETCh:WCDMa:MEASurement<Instance>:OLPControl
	CALCulate:WCDMa:MEASurement<Instance>:OLPControl



.. autoclass:: RsCmwWcdmaMeas.Implementations.OlpControl.OlpControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.olpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	OlpControl_State.rst
	OlpControl_Carrier.rst