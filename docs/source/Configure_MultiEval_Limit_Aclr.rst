Aclr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:ACLR:RELative
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:ACLR:ABSolute

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:ACLR:RELative
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:ACLR:ABSolute



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.Aclr.Aclr
	:members:
	:undoc-members:
	:noindex: