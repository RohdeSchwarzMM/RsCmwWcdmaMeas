OcInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:OCINfo
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:OCINfo

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:OCINfo
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:OCINfo



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError_.OcInfo.OcInfo
	:members:
	:undoc-members:
	:noindex: