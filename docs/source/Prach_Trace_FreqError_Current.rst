Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:FERRor:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:FERRor:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:FERRor:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:FERRor:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.FreqError_.Current.Current
	:members:
	:undoc-members:
	:noindex: