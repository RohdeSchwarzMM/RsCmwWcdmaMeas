Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.carrier.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Carrier_Trace_UePower.rst
	Tpc_Carrier_Trace_Psteps.rst