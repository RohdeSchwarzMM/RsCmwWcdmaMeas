Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:QSIGnal:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:QSIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:QSIGnal:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:QSIGnal:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.CdeMonitor_.Qsignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: