UeSignal
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UESignal:DPDCh
	single: CONFigure:WCDMa:MEASurement<Instance>:UESignal:ULConfig
	single: CONFigure:WCDMa:MEASurement<Instance>:UESignal:SFORmat
	single: CONFigure:WCDMa:MEASurement<Instance>:UESignal:CMPattern

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UESignal:DPDCh
	CONFigure:WCDMa:MEASurement<Instance>:UESignal:ULConfig
	CONFigure:WCDMa:MEASurement<Instance>:UESignal:SFORmat
	CONFigure:WCDMa:MEASurement<Instance>:UESignal:CMPattern



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeSignal.UeSignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueSignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeSignal_Carrier.rst