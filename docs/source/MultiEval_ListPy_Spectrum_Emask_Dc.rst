Dc
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask_.Dc.Dc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.emask.dc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_Emask_Dc_Current.rst
	MultiEval_ListPy_Spectrum_Emask_Dc_Average.rst
	MultiEval_ListPy_Spectrum_Emask_Dc_Maximum.rst