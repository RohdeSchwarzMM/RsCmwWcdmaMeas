Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfLeft_.Average.Average
	:members:
	:undoc-members:
	:noindex: