Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:UEPower
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:PSTeps
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:FERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:ALL
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:MERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:IQ

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:UEPower
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:PSTeps
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:FERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:ALL
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:MERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:IQ



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach_.Result.Result
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.prach.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Prach_Result_Chip.rst