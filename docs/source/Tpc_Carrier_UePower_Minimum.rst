Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.UePower_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: