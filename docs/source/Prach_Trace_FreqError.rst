FreqError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.FreqError.FreqError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_FreqError_Current.rst