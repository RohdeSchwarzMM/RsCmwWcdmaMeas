Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Iq_.Current.Current
	:members:
	:undoc-members:
	:noindex: