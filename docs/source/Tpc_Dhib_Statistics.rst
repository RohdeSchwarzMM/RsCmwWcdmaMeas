Statistics
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:DHIB:STATistics
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:STATistics

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:DHIB:STATistics
	FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:STATistics



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Dhib_.Statistics.Statistics
	:members:
	:undoc-members:
	:noindex: