Dpdch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:DPDCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:DPDCh



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.RcdError_.Eecdp_.Carrier_.Dpdch.Dpdch
	:members:
	:undoc-members:
	:noindex: