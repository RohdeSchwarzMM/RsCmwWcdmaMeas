Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:IQ:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:IQ:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:IQ:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:IQ:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Iq_.Current.Current
	:members:
	:undoc-members:
	:noindex: