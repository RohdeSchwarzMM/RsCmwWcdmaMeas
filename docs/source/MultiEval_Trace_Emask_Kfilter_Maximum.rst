Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.Kfilter_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: