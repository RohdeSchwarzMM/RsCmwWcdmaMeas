Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_UePower.rst
	MultiEval_Carrier_Trace_EvMagnitude.rst
	MultiEval_Carrier_Trace_Merror.rst
	MultiEval_Carrier_Trace_Perror.rst
	MultiEval_Carrier_Trace_CdPower.rst
	MultiEval_Carrier_Trace_CdError.rst
	MultiEval_Carrier_Trace_FreqError.rst
	MultiEval_Carrier_Trace_Psteps.rst
	MultiEval_Carrier_Trace_RcdError.rst