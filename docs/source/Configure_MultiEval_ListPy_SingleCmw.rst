SingleCmw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy_.SingleCmw.SingleCmw
	:members:
	:undoc-members:
	:noindex: