Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage
	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Psteps_.Average.Average
	:members:
	:undoc-members:
	:noindex: