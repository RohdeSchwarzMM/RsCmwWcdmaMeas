Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:PCDE:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:PCDE:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:PCDE:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:PCDE:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Pcde_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: