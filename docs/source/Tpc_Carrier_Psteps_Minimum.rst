Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum
	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Psteps_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: