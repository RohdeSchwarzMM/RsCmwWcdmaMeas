Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.multiEval.limit.rcdError.eecdp.carrier.edpdch.repcap_edpdChannel_get()
	driver.configure.multiEval.limit.rcdError.eecdp.carrier.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:EDPDch<EdpdChannel>

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:EDPDch<EdpdChannel>



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.RcdError_.Eecdp_.Carrier_.Edpdch.Edpdch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.rcdError.eecdp.carrier.edpdch.clone()