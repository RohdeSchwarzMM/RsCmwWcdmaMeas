Sf
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Sf.Sf
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.sf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Sf_Dpcch.rst
	MultiEval_Carrier_Trace_RcdError_Sf_Dpdch.rst
	MultiEval_Carrier_Trace_RcdError_Sf_Hsdpcch.rst
	MultiEval_Carrier_Trace_RcdError_Sf_Edpcch.rst
	MultiEval_Carrier_Trace_RcdError_Sf_Edpdch.rst