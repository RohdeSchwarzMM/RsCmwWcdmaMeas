Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Perror_.Chip_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: