Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: