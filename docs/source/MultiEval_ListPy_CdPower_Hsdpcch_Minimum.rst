Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:HSDPcch:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:HSDPcch:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Hsdpcch_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: