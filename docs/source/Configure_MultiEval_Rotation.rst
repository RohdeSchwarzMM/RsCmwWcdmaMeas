Rotation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:ROTation:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:ROTation:MODulation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Rotation.Rotation
	:members:
	:undoc-members:
	:noindex: