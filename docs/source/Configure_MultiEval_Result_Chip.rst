Chip
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:MERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:EVM

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:MERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CHIP:EVM



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Result_.Chip.Chip
	:members:
	:undoc-members:
	:noindex: