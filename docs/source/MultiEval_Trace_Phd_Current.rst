Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PHD:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PHD:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PHD:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PHD:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Phd_.Current.Current
	:members:
	:undoc-members:
	:noindex: