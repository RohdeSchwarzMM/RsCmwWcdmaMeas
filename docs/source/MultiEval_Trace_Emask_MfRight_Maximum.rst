Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfRight_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: