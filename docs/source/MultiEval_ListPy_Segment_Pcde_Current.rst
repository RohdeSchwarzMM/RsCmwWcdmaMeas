Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PCDE:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PCDE:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.Pcde_.Current.Current
	:members:
	:undoc-members:
	:noindex: