UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.total.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Total_UePower_Maximum.rst
	Tpc_Total_UePower_Minimum.rst
	Tpc_Total_UePower_Average.rst
	Tpc_Total_UePower_Statistics.rst