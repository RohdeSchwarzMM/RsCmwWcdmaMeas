MaxPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:URPClass
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:ACTive
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:UDEFined
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:URPClass
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:ACTive
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower:UDEFined
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:MAXPower



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach_.Limit_.Pcontrol_.MaxPower.MaxPower
	:members:
	:undoc-members:
	:noindex: