Phd
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Phd.Phd
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.phd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Phd_Current.rst