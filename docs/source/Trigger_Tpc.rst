Tpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:DELay
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:MGAP
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:SOURce
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:THReshold
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:SLOPe
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:TOUT

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:TPC:DELay
	TRIGger:WCDMa:MEASurement<Instance>:TPC:MGAP
	TRIGger:WCDMa:MEASurement<Instance>:TPC:SOURce
	TRIGger:WCDMa:MEASurement<Instance>:TPC:THReshold
	TRIGger:WCDMa:MEASurement<Instance>:TPC:SLOPe
	TRIGger:WCDMa:MEASurement<Instance>:TPC:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.Tpc.Tpc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Tpc_Catalog.rst