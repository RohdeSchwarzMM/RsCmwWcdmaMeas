Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPDch<EdpdChannel>:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPDch<EdpdChannel>:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPDch<EdpdChannel>:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:RCDerror:EDPDch<EdpdChannel>:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Edpdch_.Average.Average
	:members:
	:undoc-members:
	:noindex: