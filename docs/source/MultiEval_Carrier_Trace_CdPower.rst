CdPower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdPower.CdPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_CdPower_Dpcch.rst
	MultiEval_Carrier_Trace_CdPower_Dpdch.rst
	MultiEval_Carrier_Trace_CdPower_Hsdpcch.rst
	MultiEval_Carrier_Trace_CdPower_Edpcch.rst
	MultiEval_Carrier_Trace_CdPower_Edpdch.rst