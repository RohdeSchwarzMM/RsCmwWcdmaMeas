Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Chip_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: