Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDPower:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.CdPower_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: