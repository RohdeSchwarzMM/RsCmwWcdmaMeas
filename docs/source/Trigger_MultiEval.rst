MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:TOUT

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:DELay
	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval_Catalog.rst
	Trigger_MultiEval_ListPy.rst