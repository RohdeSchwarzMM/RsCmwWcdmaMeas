Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:UEPower:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Trace_.UePower_.Current.Current
	:members:
	:undoc-members:
	:noindex: