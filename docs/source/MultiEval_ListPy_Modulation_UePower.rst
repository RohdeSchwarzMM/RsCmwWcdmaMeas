UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_UePower_Current.rst
	MultiEval_ListPy_Modulation_UePower_Average.rst
	MultiEval_ListPy_Modulation_UePower_Maximum.rst
	MultiEval_ListPy_Modulation_UePower_StandardDev.rst