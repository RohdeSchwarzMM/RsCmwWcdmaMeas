Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimum
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimum
	FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Dhib_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: