Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfLeft_.Average.Average
	:members:
	:undoc-members:
	:noindex: