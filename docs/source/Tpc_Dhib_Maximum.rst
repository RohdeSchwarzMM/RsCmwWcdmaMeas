Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum
	CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Dhib_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: