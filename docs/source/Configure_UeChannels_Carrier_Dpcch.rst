Dpcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:DPCCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:DPCCh



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels_.Carrier_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex: