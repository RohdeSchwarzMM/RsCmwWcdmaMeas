MaProtocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:WCDMa:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwWcdmaMeas.Implementations.Route_.Scenario_.MaProtocol.MaProtocol
	:members:
	:undoc-members:
	:noindex: