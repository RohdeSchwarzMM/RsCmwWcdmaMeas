UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Trace_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.carrier.trace.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Carrier_Trace_UePower_Current.rst