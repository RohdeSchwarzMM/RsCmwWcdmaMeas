Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:CDERror:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:CDERror:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:CDERror:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:CDERror:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.CdError_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: