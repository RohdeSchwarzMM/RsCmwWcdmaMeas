Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDEMonitor:ISIGnal:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.CdeMonitor_.Isignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: