Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:AVERage
	single: READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:AVERage
	READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.UePower_.Average.Average
	:members:
	:undoc-members:
	:noindex: