Isignal
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.CdeMonitor_.Isignal.Isignal
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.cdeMonitor.isignal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_CdeMonitor_Isignal_Current.rst