MaxPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:URPClass
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:ACTive
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:UDEFined
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:URPClass
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:ACTive
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower:UDEFined
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MAXPower



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Limit_.IlpControl_.MaxPower.MaxPower
	:members:
	:undoc-members:
	:noindex: