Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:CHIP:CURRent
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:CHIP:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:CHIP:CURRent
	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:CHIP:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Merror_.Chip_.Current.Current
	:members:
	:undoc-members:
	:noindex: