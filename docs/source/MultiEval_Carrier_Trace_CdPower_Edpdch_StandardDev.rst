StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:EDPDch<EdpdChannel>:SDEViation
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:EDPDch<EdpdChannel>:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:EDPDch<EdpdChannel>:SDEViation
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:EDPDch<EdpdChannel>:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdPower_.Edpdch_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: