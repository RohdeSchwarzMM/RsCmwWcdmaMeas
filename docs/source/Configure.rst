Configure
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Carrier.rst
	Configure_Cell.rst
	Configure_UeSignal.rst
	Configure_UeChannels.rst
	Configure_RfSettings.rst
	Configure_MultiEval.rst
	Configure_Tpc.rst
	Configure_Prach.rst
	Configure_OoSync.rst
	Configure_OlpControl.rst