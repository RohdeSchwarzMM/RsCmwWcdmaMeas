Pcontrol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PCONtrol:HSDPcch
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PCONtrol:EPSTep

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PCONtrol:HSDPcch
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PCONtrol:EPSTep



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.Pcontrol.Pcontrol
	:members:
	:undoc-members:
	:noindex: