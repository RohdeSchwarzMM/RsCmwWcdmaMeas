P<Plus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.multiEval.listPy.spectrum.aclr.p.repcap_plus_get()
	driver.multiEval.listPy.spectrum.aclr.p.repcap_plus_set(repcap.Plus.Ch1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Aclr_.P.P
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.aclr.p.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_Aclr_P_Current.rst
	MultiEval_ListPy_Spectrum_Aclr_P_Average.rst
	MultiEval_ListPy_Spectrum_Aclr_P_Maximum.rst