StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:TTERror:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:TTERror:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.TtError_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: