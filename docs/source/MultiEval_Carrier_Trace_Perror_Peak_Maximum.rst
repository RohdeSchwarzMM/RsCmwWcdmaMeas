Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:MAXimum
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Perror_.Peak_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: