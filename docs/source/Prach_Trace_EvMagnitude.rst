EvMagnitude
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.EvMagnitude.EvMagnitude
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_EvMagnitude_Rms.rst
	Prach_Trace_EvMagnitude_Peak.rst
	Prach_Trace_EvMagnitude_Chip.rst