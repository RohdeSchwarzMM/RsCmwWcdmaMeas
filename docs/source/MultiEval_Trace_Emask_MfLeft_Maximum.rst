Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfLeft_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: