Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfRight_.Average.Average
	:members:
	:undoc-members:
	:noindex: