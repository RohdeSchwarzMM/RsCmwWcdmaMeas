Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_UePower.rst
	Prach_Trace_EvMagnitude.rst
	Prach_Trace_Merror.rst
	Prach_Trace_Perror.rst
	Prach_Trace_FreqError.rst
	Prach_Trace_Psteps.rst
	Prach_Trace_Iq.rst