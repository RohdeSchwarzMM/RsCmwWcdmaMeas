Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.carrier.repcap_carrier_get()
	driver.multiEval.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace.rst
	MultiEval_Carrier_Modulation.rst
	MultiEval_Carrier_RcdError.rst
	MultiEval_Carrier_CdPower.rst
	MultiEval_Carrier_CdError.rst