IlpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MINPower
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:PSTep
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:EPSTep
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:PSGRoup

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:MINPower
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:PSTep
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:EPSTep
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ILPControl:PSGRoup



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Limit_.IlpControl.IlpControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tpc.limit.ilpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tpc_Limit_IlpControl_MaxPower.rst