UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.Trace_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.total.trace.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Total_Trace_UePower_Current.rst