HkfRight
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfRight.HkfRight
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.emask.hkfRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Emask_HkfRight_Average.rst
	MultiEval_Trace_Emask_HkfRight_Current.rst
	MultiEval_Trace_Emask_HkfRight_Maximum.rst