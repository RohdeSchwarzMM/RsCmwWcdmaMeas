Edpdch<EdpdChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.listPy.cdError.edpdch.repcap_edpdChannel_get()
	driver.multiEval.listPy.cdError.edpdch.repcap_edpdChannel_set(repcap.EdpdChannel.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Edpdch.Edpdch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdError.edpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdError_Edpdch_Current.rst
	MultiEval_ListPy_CdError_Edpdch_Average.rst
	MultiEval_ListPy_CdError_Edpdch_Maximum.rst
	MultiEval_ListPy_CdError_Edpdch_StandardDev.rst