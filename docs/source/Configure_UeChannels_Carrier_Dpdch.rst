Dpdch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:DPDCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:DPDCh



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels_.Carrier_.Dpdch.Dpdch
	:members:
	:undoc-members:
	:noindex: