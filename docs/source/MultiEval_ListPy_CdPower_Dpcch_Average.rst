Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:DPCCh:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:DPCCh:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Dpcch_.Average.Average
	:members:
	:undoc-members:
	:noindex: