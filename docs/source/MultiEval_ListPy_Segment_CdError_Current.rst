Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDERror:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDERror:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.CdError_.Current.Current
	:members:
	:undoc-members:
	:noindex: