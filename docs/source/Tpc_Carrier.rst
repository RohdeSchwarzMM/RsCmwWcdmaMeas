Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.tpc.carrier.repcap_carrier_get()
	driver.tpc.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Carrier_Psteps.rst
	Tpc_Carrier_UePower.rst
	Tpc_Carrier_Trace.rst