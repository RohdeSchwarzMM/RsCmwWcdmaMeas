Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:MODulation:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: