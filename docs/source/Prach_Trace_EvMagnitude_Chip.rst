Chip
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.EvMagnitude_.Chip.Chip
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.evMagnitude.chip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_EvMagnitude_Chip_Current.rst