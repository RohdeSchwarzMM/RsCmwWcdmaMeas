State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:OOSYnc:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:OOSYnc:STATe



.. autoclass:: RsCmwWcdmaMeas.Implementations.OoSync_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ooSync.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	OoSync_State_All.rst