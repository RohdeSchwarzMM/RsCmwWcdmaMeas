CdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.CdError.CdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_CdError_Current.rst
	MultiEval_Carrier_CdError_Average.rst
	MultiEval_Carrier_CdError_Maximum.rst
	MultiEval_Carrier_CdError_StandardDev.rst