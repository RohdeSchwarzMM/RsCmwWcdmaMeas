Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:BER
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:BER

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:BER
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:BER



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: