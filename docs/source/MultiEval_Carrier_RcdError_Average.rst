Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError_.Average.Average
	:members:
	:undoc-members:
	:noindex: