Rup<RampUpCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.olpControl.carrier.uepPower.rup.repcap_rampUpCarrier_get()
	driver.olpControl.carrier.uepPower.rup.repcap_rampUpCarrier_set(repcap.RampUpCarrier.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:OLPControl:CARRier<CARRierExt>:UEPPower:RUP<RampUpCarrier>
	single: FETCh:WCDMa:MEASurement<Instance>:OLPControl:CARRier<CARRierExt>:UEPPower:RUP<RampUpCarrier>

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:OLPControl:CARRier<CARRierExt>:UEPPower:RUP<RampUpCarrier>
	FETCh:WCDMa:MEASurement<Instance>:OLPControl:CARRier<CARRierExt>:UEPPower:RUP<RampUpCarrier>



.. autoclass:: RsCmwWcdmaMeas.Implementations.OlpControl_.Carrier_.UepPower_.Rup.Rup
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.olpControl.carrier.uepPower.rup.clone()