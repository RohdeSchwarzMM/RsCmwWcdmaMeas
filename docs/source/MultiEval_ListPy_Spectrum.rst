Spectrum
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum.Spectrum
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_UePower.rst
	MultiEval_ListPy_Spectrum_Emask.rst
	MultiEval_ListPy_Spectrum_Cpower.rst
	MultiEval_ListPy_Spectrum_Aclr.rst
	MultiEval_ListPy_Spectrum_Obw.rst
	MultiEval_ListPy_Spectrum_Current.rst
	MultiEval_ListPy_Spectrum_Average.rst
	MultiEval_ListPy_Spectrum_Maximum.rst