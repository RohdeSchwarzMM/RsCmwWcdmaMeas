State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:STATe



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_State_All.rst