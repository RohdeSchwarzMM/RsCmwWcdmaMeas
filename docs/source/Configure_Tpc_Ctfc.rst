Ctfc
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Ctfc.Ctfc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tpc.ctfc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tpc_Ctfc_Mlength.rst