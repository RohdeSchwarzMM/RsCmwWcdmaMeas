Pcontrol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:PSTep
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:OLPower
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:OFFPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:PSTep
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:OLPower
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PCONtrol:OFFPower



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach_.Limit_.Pcontrol.Pcontrol
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.prach.limit.pcontrol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Prach_Limit_Pcontrol_MaxPower.rst