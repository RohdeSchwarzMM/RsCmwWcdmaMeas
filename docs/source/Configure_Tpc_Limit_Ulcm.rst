Ulcm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ULCM:PA
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ULCM:PB

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ULCM:PA
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:ULCM:PB



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Limit_.Ulcm.Ulcm
	:members:
	:undoc-members:
	:noindex: