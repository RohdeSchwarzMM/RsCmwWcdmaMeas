CdPower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.CdPower.CdPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_CdPower_Current.rst
	MultiEval_Carrier_CdPower_Average.rst
	MultiEval_Carrier_CdPower_Minimum.rst
	MultiEval_Carrier_CdPower_Maximum.rst
	MultiEval_Carrier_CdPower_StandardDev.rst