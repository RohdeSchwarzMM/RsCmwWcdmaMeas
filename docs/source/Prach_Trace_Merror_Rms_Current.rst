Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:RMS:CURRent
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:RMS:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:RMS:CURRent
	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:MERRor:RMS:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Merror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: