Scount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:BER
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:BER
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Scount.Scount
	:members:
	:undoc-members:
	:noindex: