ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODE



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex: