OoSync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:DELay
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:MGAP
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:SOURce
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:THReshold
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:SLOPe
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:TOUT

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:DELay
	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:MGAP
	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:SOURce
	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:THReshold
	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:SLOPe
	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.OoSync.OoSync
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_OoSync_Catalog.rst