M<Minus>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.multiEval.listPy.spectrum.aclr.m.repcap_minus_get()
	driver.multiEval.listPy.spectrum.aclr.m.repcap_minus_set(repcap.Minus.Ch1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Aclr_.M.M
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.aclr.m.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_Aclr_M_Current.rst
	MultiEval_ListPy_Spectrum_Aclr_M_Average.rst
	MultiEval_ListPy_Spectrum_Aclr_M_Maximum.rst