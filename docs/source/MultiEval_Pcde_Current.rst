Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:PCDE:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:PCDE:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:PCDE:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:PCDE:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Pcde_.Current.Current
	:members:
	:undoc-members:
	:noindex: