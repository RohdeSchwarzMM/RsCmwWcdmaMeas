Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:QSIGnal:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.CdpMonitor_.Qsignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: