Scode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:CELL:CARRier<Carrier>:SCODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:CELL:CARRier<Carrier>:SCODe



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Cell_.Carrier_.Scode.Scode
	:members:
	:undoc-members:
	:noindex: