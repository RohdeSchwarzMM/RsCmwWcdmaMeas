Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.FreqError_.Current.Current
	:members:
	:undoc-members:
	:noindex: