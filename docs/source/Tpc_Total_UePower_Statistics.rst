Statistics
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:STATistics
	single: READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:STATistics

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:STATistics
	READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:STATistics



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.UePower_.Statistics.Statistics
	:members:
	:undoc-members:
	:noindex: