Rms
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.EvMagnitude_.Rms.Rms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.evMagnitude.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_EvMagnitude_Rms_Sdeviaton.rst
	MultiEval_Carrier_Trace_EvMagnitude_Rms_StandardDev.rst
	MultiEval_Carrier_Trace_EvMagnitude_Rms_Maximum.rst
	MultiEval_Carrier_Trace_EvMagnitude_Rms_Average.rst
	MultiEval_Carrier_Trace_EvMagnitude_Rms_Current.rst