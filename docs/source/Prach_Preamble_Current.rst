Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent
	single: CALCulate:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent
	CALCulate:WCDMa:MEASurement<Instance>:PRACh:PREamble<Preamble>:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Preamble_.Current.Current
	:members:
	:undoc-members:
	:noindex: