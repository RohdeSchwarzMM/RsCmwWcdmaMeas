Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum
	READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Spectrum_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: