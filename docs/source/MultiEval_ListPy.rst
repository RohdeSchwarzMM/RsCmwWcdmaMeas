ListPy
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Sreliability.rst
	MultiEval_ListPy_UePower.rst
	MultiEval_ListPy_Segment.rst
	MultiEval_ListPy_Phd.rst
	MultiEval_ListPy_Pcde.rst
	MultiEval_ListPy_CdPower.rst
	MultiEval_ListPy_Spectrum.rst
	MultiEval_ListPy_Modulation.rst
	MultiEval_ListPy_CdError.rst