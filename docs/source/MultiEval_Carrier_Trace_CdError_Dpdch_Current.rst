Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:DPDCh:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:DPDCh:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:DPDCh:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:DPDCh:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdError_.Dpdch_.Current.Current
	:members:
	:undoc-members:
	:noindex: