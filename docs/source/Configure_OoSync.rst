OoSync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:AADPchlevel
	single: CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:TOUT

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:AADPchlevel
	CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.OoSync.OoSync
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_OoSync_Limit.rst