Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:HSDPcch:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:HSDPcch:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Hsdpcch_.Current.Current
	:members:
	:undoc-members:
	:noindex: