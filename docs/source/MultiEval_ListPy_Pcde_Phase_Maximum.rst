Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:PCDE:PHASe:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:PCDE:PHASe:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Pcde_.Phase_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: