Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:MEASurement<Instance>:SCENario:SALone
	single: ROUTe:WCDMa:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:WCDMa:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:WCDMa:MEASurement<Instance>:SCENario:SALone
	ROUTe:WCDMa:MEASurement<Instance>:SCENario:CSPath
	ROUTe:WCDMa:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwWcdmaMeas.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_MaProtocol.rst