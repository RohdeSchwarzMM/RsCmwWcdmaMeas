Ulcm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ULCM:MLENgth
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ULCM:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:ULCM:MLENgth
	CONFigure:WCDMa:MEASurement<Instance>:TPC:ULCM:AEXecution



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Ulcm.Ulcm
	:members:
	:undoc-members:
	:noindex: