Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:DPCCh:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:DPCCh:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Dpcch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: