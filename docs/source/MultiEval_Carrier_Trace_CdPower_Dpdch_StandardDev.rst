StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPDCh:SDEViation
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPDCh:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPDCh:SDEViation
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDPower:DPDCh:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdPower_.Dpdch_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: