Hsdpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Hsdpcch.Hsdpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdPower.hsdpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdPower_Hsdpcch_Current.rst
	MultiEval_ListPy_CdPower_Hsdpcch_Average.rst
	MultiEval_ListPy_CdPower_Hsdpcch_Minimum.rst
	MultiEval_ListPy_CdPower_Hsdpcch_Maximum.rst
	MultiEval_ListPy_CdPower_Hsdpcch_StandardDev.rst