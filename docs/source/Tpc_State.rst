State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:STATe

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:STATe



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_State_All.rst