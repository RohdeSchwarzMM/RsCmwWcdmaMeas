Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:PONupper
	single: CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:POFFupper
	single: CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:THReshold

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:PONupper
	CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:POFFupper
	CONFigure:WCDMa:MEASurement<Instance>:OOSYnc:LIMit:THReshold



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.OoSync_.Limit.Limit
	:members:
	:undoc-members:
	:noindex: