Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:EDPDch<EdpdChannel>:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:EDPDch<EdpdChannel>:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Edpdch_.Average.Average
	:members:
	:undoc-members:
	:noindex: