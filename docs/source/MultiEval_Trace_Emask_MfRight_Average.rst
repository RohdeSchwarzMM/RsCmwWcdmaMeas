Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfRight_.Average.Average
	:members:
	:undoc-members:
	:noindex: