RsCmwWcdmaMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwWcdmaMeas('TCPIP::192.168.2.101::HISLIP')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwWcdmaMeas.RsCmwWcdmaMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Route.rst
	Configure.rst
	Trigger.rst
	MultiEval.rst
	Tpc.rst
	Prach.rst
	OoSync.rst
	OlpControl.rst