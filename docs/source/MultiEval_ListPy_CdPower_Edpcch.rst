Edpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Edpcch.Edpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdPower.edpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdPower_Edpcch_Current.rst
	MultiEval_ListPy_CdPower_Edpcch_Average.rst
	MultiEval_ListPy_CdPower_Edpcch_Minimum.rst
	MultiEval_ListPy_CdPower_Edpcch_Maximum.rst
	MultiEval_ListPy_CdPower_Edpcch_StandardDev.rst