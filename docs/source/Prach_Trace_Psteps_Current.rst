Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PSTeps:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PSTeps:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PSTeps:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PSTeps:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Psteps_.Current.Current
	:members:
	:undoc-members:
	:noindex: