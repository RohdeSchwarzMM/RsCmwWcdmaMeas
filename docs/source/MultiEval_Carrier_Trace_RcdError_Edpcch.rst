Edpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Edpcch.Edpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.edpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Edpcch_Current.rst
	MultiEval_Carrier_Trace_RcdError_Edpcch_Average.rst
	MultiEval_Carrier_Trace_RcdError_Edpcch_Maximum.rst
	MultiEval_Carrier_Trace_RcdError_Edpcch_StandardDev.rst