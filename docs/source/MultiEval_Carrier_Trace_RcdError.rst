RcdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError.RcdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Sf.rst
	MultiEval_Carrier_Trace_RcdError_Dpcch.rst
	MultiEval_Carrier_Trace_RcdError_Dpdch.rst
	MultiEval_Carrier_Trace_RcdError_Hsdpcch.rst
	MultiEval_Carrier_Trace_RcdError_Edpcch.rst
	MultiEval_Carrier_Trace_RcdError_Edpdch.rst