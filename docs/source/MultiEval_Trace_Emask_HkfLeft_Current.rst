Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFLeft:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfLeft_.Current.Current
	:members:
	:undoc-members:
	:noindex: