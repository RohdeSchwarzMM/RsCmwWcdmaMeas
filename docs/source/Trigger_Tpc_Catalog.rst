Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:TPC:CATalog:SOURce



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.Tpc_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: