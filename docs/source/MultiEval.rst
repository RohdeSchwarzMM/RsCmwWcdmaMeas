MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:MEASurement<Instance>:MEValuation
	single: ABORt:WCDMa:MEASurement<Instance>:MEValuation
	single: INITiate:WCDMa:MEASurement<Instance>:MEValuation

.. code-block:: python

	STOP:WCDMa:MEASurement<Instance>:MEValuation
	ABORt:WCDMa:MEASurement<Instance>:MEValuation
	INITiate:WCDMa:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_State.rst
	MultiEval_Trace.rst
	MultiEval_Carrier.rst
	MultiEval_Spectrum.rst
	MultiEval_Modulation.rst
	MultiEval_Ber.rst
	MultiEval_Pcde.rst
	MultiEval_ListPy.rst