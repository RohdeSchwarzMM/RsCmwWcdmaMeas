Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:HKFRight:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.HkfRight_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: