UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_UePower_Current.rst
	Prach_Trace_UePower_Chip.rst