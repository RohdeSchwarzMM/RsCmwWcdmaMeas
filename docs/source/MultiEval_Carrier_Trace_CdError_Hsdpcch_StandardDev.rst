StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:HSDPcch:SDEViation
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:HSDPcch:SDEViation

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:HSDPcch:SDEViation
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:CDERror:HSDPcch:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdError_.Hsdpcch_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: