Prach
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:DELay
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:MGAP
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:SOURce
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:TOUT

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:PRACh:DELay
	TRIGger:WCDMa:MEASurement<Instance>:PRACh:MGAP
	TRIGger:WCDMa:MEASurement<Instance>:PRACh:SOURce
	TRIGger:WCDMa:MEASurement<Instance>:PRACh:THReshold
	TRIGger:WCDMa:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:WCDMa:MEASurement<Instance>:PRACh:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.Prach.Prach
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Prach_Catalog.rst