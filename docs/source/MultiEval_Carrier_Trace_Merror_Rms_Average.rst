Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:RMS:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:RMS:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:RMS:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:RMS:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Merror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: