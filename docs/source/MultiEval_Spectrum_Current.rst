Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Spectrum_.Current.Current
	:members:
	:undoc-members:
	:noindex: