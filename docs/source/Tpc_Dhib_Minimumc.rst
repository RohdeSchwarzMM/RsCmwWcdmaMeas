Minimumc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimumc

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:MINimumc



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Dhib_.Minimumc.Minimumc
	:members:
	:undoc-members:
	:noindex: