MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:MSCount
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:PSLot
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SYNCh
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:REPetition

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:MSCount
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:PSLot
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SYNCh
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:REPetition



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Scount.rst
	Configure_MultiEval_Limit.rst
	Configure_MultiEval_Rotation.rst
	Configure_MultiEval_DsFactor.rst
	Configure_MultiEval_Sscalar.rst
	Configure_MultiEval_CdThreshold.rst
	Configure_MultiEval_Dmode.rst
	Configure_MultiEval_Amode.rst
	Configure_MultiEval_Mperiod.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_ListPy.rst