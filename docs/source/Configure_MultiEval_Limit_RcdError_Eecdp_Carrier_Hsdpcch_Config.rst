Config
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:HSDPcch:CONFig

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:HSDPcch:CONFig



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.RcdError_.Eecdp_.Carrier_.Hsdpcch_.Config.Config
	:members:
	:undoc-members:
	:noindex: