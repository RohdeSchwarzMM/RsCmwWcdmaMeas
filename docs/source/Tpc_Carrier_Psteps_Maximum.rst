Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:PSTeps:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Psteps_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: