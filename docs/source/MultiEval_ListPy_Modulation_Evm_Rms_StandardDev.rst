StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: