Dpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.dpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Dpcch_Current.rst
	MultiEval_Carrier_Trace_RcdError_Dpcch_Average.rst
	MultiEval_Carrier_Trace_RcdError_Dpcch_Maximum.rst
	MultiEval_Carrier_Trace_RcdError_Dpcch_StandardDev.rst