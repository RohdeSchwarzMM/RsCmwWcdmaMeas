Dpdch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Dpdch.Dpdch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdError.dpdch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdError_Dpdch_Current.rst
	MultiEval_ListPy_CdError_Dpdch_Average.rst
	MultiEval_ListPy_CdError_Dpdch_Maximum.rst
	MultiEval_ListPy_CdError_Dpdch_StandardDev.rst