Merror
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Merror.Merror
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_Merror_Rms.rst
	Prach_Trace_Merror_Peak.rst
	Prach_Trace_Merror_Chip.rst