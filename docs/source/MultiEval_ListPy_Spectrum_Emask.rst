Emask
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask.Emask
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.emask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_Emask_Hda.rst
	MultiEval_ListPy_Spectrum_Emask_Had.rst
	MultiEval_ListPy_Spectrum_Emask_Ab.rst
	MultiEval_ListPy_Spectrum_Emask_Bc.rst
	MultiEval_ListPy_Spectrum_Emask_Cd.rst
	MultiEval_ListPy_Spectrum_Emask_Ef.rst
	MultiEval_ListPy_Spectrum_Emask_Fe.rst
	MultiEval_ListPy_Spectrum_Emask_Dc.rst
	MultiEval_ListPy_Spectrum_Emask_Cb.rst
	MultiEval_ListPy_Spectrum_Emask_Ba.rst