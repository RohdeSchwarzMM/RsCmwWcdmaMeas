Dcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:RFSettings:DCARrier:SEParation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:RFSettings:DCARrier:SEParation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.RfSettings_.Dcarrier.Dcarrier
	:members:
	:undoc-members:
	:noindex: