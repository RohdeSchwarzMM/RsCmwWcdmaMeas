Config
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:HSDPcch:CONFig

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:HSDPcch:CONFig



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels_.Carrier_.Hsdpcch_.Config.Config
	:members:
	:undoc-members:
	:noindex: