Dpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdPower_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.cdPower.dpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_CdPower_Dpcch_Current.rst
	MultiEval_Carrier_Trace_CdPower_Dpcch_Average.rst
	MultiEval_Carrier_Trace_CdPower_Dpcch_Minimum.rst
	MultiEval_Carrier_Trace_CdPower_Dpcch_Maximum.rst
	MultiEval_Carrier_Trace_CdPower_Dpcch_StandardDev.rst