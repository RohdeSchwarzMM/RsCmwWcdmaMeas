FreqError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.FreqError.FreqError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_FreqError_StandardDev.rst
	MultiEval_Carrier_Trace_FreqError_Maximum.rst
	MultiEval_Carrier_Trace_FreqError_Average.rst
	MultiEval_Carrier_Trace_FreqError_Current.rst