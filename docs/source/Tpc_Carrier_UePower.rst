UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.carrier.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Carrier_UePower_Maximum.rst
	Tpc_Carrier_UePower_Minimum.rst
	Tpc_Carrier_UePower_Average.rst
	Tpc_Carrier_UePower_Statistics.rst