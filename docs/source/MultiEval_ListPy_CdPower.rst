CdPower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower.CdPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdPower_Dpcch.rst
	MultiEval_ListPy_CdPower_Dpdch.rst
	MultiEval_ListPy_CdPower_Hsdpcch.rst
	MultiEval_ListPy_CdPower_Edpcch.rst
	MultiEval_ListPy_CdPower_Edpdch.rst
	MultiEval_ListPy_CdPower_StandardDev.rst
	MultiEval_ListPy_CdPower_Maximum.rst
	MultiEval_ListPy_CdPower_Minimum.rst
	MultiEval_ListPy_CdPower_Average.rst
	MultiEval_ListPy_CdPower_Current.rst