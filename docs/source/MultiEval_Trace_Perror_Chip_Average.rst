Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Perror_.Chip_.Average.Average
	:members:
	:undoc-members:
	:noindex: