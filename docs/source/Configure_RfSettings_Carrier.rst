Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.rfSettings.carrier.repcap_carrier_get()
	driver.configure.rfSettings.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.RfSettings_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Carrier_Frequency.rst