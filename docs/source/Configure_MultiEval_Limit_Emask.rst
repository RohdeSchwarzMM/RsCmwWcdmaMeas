Emask
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:ABSolute
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:RELative

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:ABSolute
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:RELative



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.Emask.Emask
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.emask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Emask_Dcarrier.rst