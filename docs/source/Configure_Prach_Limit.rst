Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:MERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:IQOFfset
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:IQIMbalance
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:CFERror

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:EVMagnitude
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:MERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:IQOFfset
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:IQIMbalance
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:LIMit:CFERror



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.prach.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Prach_Limit_Pcontrol.rst