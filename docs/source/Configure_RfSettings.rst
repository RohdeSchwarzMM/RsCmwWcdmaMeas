RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:WCDMa:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:WCDMa:MEASurement<Instance>:RFSettings:ENPower

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:WCDMa:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:WCDMa:MEASurement<Instance>:RFSettings:ENPower



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dcarrier.rst
	Configure_RfSettings_Carrier.rst