CdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError.CdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdError_Dpcch.rst
	MultiEval_ListPy_CdError_Dpdch.rst
	MultiEval_ListPy_CdError_Hsdpcch.rst
	MultiEval_ListPy_CdError_Edpcch.rst
	MultiEval_ListPy_CdError_Edpdch.rst
	MultiEval_ListPy_CdError_Current.rst
	MultiEval_ListPy_CdError_Average.rst
	MultiEval_ListPy_CdError_Maximum.rst
	MultiEval_ListPy_CdError_StandardDev.rst