Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFLeft:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfLeft_.Current.Current
	:members:
	:undoc-members:
	:noindex: