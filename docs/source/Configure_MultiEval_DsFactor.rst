DsFactor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:DSFactor:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:DSFactor:MODulation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.DsFactor.DsFactor
	:members:
	:undoc-members:
	:noindex: