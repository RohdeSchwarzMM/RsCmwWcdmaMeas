Uephd
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd
	READ:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:MODulation:UEPHd



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Modulation_.Uephd.Uephd
	:members:
	:undoc-members:
	:noindex: