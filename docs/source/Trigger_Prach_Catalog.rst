Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.Prach_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: