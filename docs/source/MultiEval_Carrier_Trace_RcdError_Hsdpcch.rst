Hsdpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.RcdError_.Hsdpcch.Hsdpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.rcdError.hsdpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_RcdError_Hsdpcch_Current.rst
	MultiEval_Carrier_Trace_RcdError_Hsdpcch_Average.rst
	MultiEval_Carrier_Trace_RcdError_Hsdpcch_Maximum.rst
	MultiEval_Carrier_Trace_RcdError_Hsdpcch_StandardDev.rst