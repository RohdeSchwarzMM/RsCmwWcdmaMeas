Peak
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Merror_.Peak.Peak
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.merror.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_Merror_Peak_StandardDev.rst
	MultiEval_Carrier_Trace_Merror_Peak_Maximum.rst
	MultiEval_Carrier_Trace_Merror_Peak_Average.rst
	MultiEval_Carrier_Trace_Merror_Peak_Current.rst