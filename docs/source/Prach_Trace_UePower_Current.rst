Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.UePower_.Current.Current
	:members:
	:undoc-members:
	:noindex: