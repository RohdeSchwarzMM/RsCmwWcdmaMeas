StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: