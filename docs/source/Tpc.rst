Tpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:MEASurement<Instance>:TPC
	single: ABORt:WCDMa:MEASurement<Instance>:TPC
	single: INITiate:WCDMa:MEASurement<Instance>:TPC

.. code-block:: python

	STOP:WCDMa:MEASurement<Instance>:TPC
	ABORt:WCDMa:MEASurement<Instance>:TPC
	INITiate:WCDMa:MEASurement<Instance>:TPC



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc.Tpc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Dhib.rst
	Tpc_Carrier.rst
	Tpc_Total.rst
	Tpc_State.rst