CdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.CdError.CdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_CdError_Dpcch.rst
	MultiEval_Carrier_Trace_CdError_Dpdch.rst
	MultiEval_Carrier_Trace_CdError_Hsdpcch.rst
	MultiEval_Carrier_Trace_CdError_Edpcch.rst
	MultiEval_Carrier_Trace_CdError_Edpdch.rst