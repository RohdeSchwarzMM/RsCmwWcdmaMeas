Chip
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Chip.Chip
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.evMagnitude.chip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_EvMagnitude_Chip_Maximum.rst
	MultiEval_Trace_EvMagnitude_Chip_Average.rst
	MultiEval_Trace_EvMagnitude_Chip_Current.rst