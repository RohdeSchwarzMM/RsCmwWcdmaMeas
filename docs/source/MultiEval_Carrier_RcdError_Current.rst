Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError_.Current.Current
	:members:
	:undoc-members:
	:noindex: