Mpedch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:MPEDch:MLENgth
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:MPEDch:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:MPEDch:MLENgth
	CONFigure:WCDMa:MEASurement<Instance>:TPC:MPEDch:AEXecution



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Mpedch.Mpedch
	:members:
	:undoc-members:
	:noindex: