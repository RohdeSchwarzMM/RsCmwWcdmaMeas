OoSync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:OOSYnc
	single: READ:WCDMa:MEASurement<Instance>:OOSYnc
	single: FETCh:WCDMa:MEASurement<Instance>:OOSYnc
	single: STOP:WCDMa:MEASurement<Instance>:OOSYnc
	single: ABORt:WCDMa:MEASurement<Instance>:OOSYnc
	single: INITiate:WCDMa:MEASurement<Instance>:OOSYnc

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:OOSYnc
	READ:WCDMa:MEASurement<Instance>:OOSYnc
	FETCh:WCDMa:MEASurement<Instance>:OOSYnc
	STOP:WCDMa:MEASurement<Instance>:OOSYnc
	ABORt:WCDMa:MEASurement<Instance>:OOSYnc
	INITiate:WCDMa:MEASurement<Instance>:OOSYnc



.. autoclass:: RsCmwWcdmaMeas.Implementations.OoSync.OoSync
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ooSync.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	OoSync_State.rst