Enums
=========

AclrMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AclrMode.ABSolute
	# All values (2x):
	ABSolute | RELative

ActiveLimit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ActiveLimit.PC1
	# All values (6x):
	PC1 | PC2 | PC3 | PC3B | PC4 | USER

AnalysisMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AnalysisMode.NOOFfset
	# All values (2x):
	NOOFfset | WOOFfset

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.OB1
	# Last value:
	value = enums.Band.OBS3
	# All values (28x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB25
	OB26 | OB3 | OB4 | OB5 | OB6 | OB7 | OB8 | OB9
	OBL1 | OBS1 | OBS2 | OBS3

Carrier
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Carrier.C1
	# All values (2x):
	C1 | C2

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RH8
	# All values (96x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8
	RC1 | RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8
	RD1 | RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8
	RE1 | RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8
	RF1 | RF2 | RF3 | RF4 | RF5 | RF6 | RF7 | RF8
	RG1 | RG2 | RG3 | RG4 | RG5 | RG6 | RG7 | RG8
	RH1 | RH2 | RH3 | RH4 | RH5 | RH6 | RH7 | RH8

DetectionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectionMode.A3G
	# All values (1x):
	A3G

LimitHmode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LimitHmode.A
	# All values (3x):
	A | B | C

MeasMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasMode.CTFC
	# All values (6x):
	CTFC | DHIB | ILPControl | MONitor | MPEDch | ULCM

MeasPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasPeriod.FULLslot
	# All values (2x):
	FULLslot | HALFslot

Mode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mode.ONCE
	# All values (2x):
	ONCE | SEGMent

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation._4PAM
	# All values (5x):
	_4PAM | _4PVar | BPSK | BVAR | OFF

OutPowFstate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutPowFstate.NOFF
	# All values (4x):
	NOFF | NON | OFF | ON

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternType.AF
	# All values (3x):
	AF | AR | B

PcdErrorPhase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PcdErrorPhase.IPHase
	# All values (2x):
	IPHase | QPHase

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

Retrigger
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Retrigger.IFPower
	# All values (4x):
	IFPower | IFPSync | OFF | ON

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

SetType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SetType.ALL0
	# Last value:
	value = enums.SetType.ULCM
	# All values (19x):
	ALL0 | ALL1 | ALTernating | CLOop | CONTinuous | CTFC | DHIB | MPEDch
	PHDown | PHUP | SAL0 | SAL1 | SALT | TSABc | TSE | TSEF
	TSF | TSGH | ULCM

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SlotNumber
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SlotNumber.ANY
	# Last value:
	value = enums.SlotNumber.SL9
	# All values (16x):
	ANY | SL0 | SL1 | SL10 | SL11 | SL12 | SL13 | SL14
	SL2 | SL3 | SL4 | SL5 | SL6 | SL7 | SL8 | SL9

SpreadingFactorA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpreadingFactorA.SF128
	# All values (7x):
	SF128 | SF16 | SF256 | SF32 | SF4 | SF64 | SF8

SpreadingFactorB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SpreadingFactorB._128
	# Last value:
	value = enums.SpreadingFactorB.V8
	# All values (16x):
	_128 | _16 | _2 | _256 | _32 | _4 | _64 | _8
	V128 | V16 | V2 | V256 | V32 | V4 | V64 | V8

State
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.State.OFF
	# All values (3x):
	OFF | ON | VAR

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

TestCase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestCase.T0DB
	# All values (2x):
	T0DB | T1DB

TestScenarioB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestScenarioB.CSPath
	# All values (4x):
	CSPath | MAPRotocol | SALone | UNDefined

Type
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Type.ACK
	# All values (3x):
	ACK | CQI | NACK

UlConfiguration
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UlConfiguration._3CHS
	# Last value:
	value = enums.UlConfiguration.WCDMa
	# All values (16x):
	_3CHS | _3DUPlus | _3HDU | _4CHS | _4DUPlus | _4HDU | DCHS | DDUPlus
	DHDU | HDUPlus | HSDPa | HSPA | HSPLus | HSUPa | QPSK | WCDMa

