Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Merror_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: