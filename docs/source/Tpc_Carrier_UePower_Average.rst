Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.UePower_.Average.Average
	:members:
	:undoc-members:
	:noindex: