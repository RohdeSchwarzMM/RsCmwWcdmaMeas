Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:MPEDch
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:CTFC
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:DHIB

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:MPEDch
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:CTFC
	CONFigure:WCDMa:MEASurement<Instance>:TPC:LIMit:DHIB



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tpc.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tpc_Limit_IlpControl.rst
	Configure_Tpc_Limit_Ulcm.rst