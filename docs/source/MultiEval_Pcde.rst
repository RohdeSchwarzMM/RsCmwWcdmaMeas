Pcde
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Pcde.Pcde
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pcde.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pcde_Current.rst
	MultiEval_Pcde_Maximum.rst