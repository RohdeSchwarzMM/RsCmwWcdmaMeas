StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:SDEViation
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:SDEViation
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:PEAK:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Perror_.Peak_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: