Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MINimum
	single: READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MINimum
	READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.UePower_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: