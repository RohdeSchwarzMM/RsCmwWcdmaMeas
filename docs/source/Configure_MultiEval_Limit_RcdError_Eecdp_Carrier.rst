Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.multiEval.limit.rcdError.eecdp.carrier.repcap_carrier_get()
	driver.configure.multiEval.limit.rcdError.eecdp.carrier.repcap_carrier_set(repcap.Carrier.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.RcdError_.Eecdp_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.rcdError.eecdp.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_RcdError_Eecdp_Carrier_Edpdch.rst
	Configure_MultiEval_Limit_RcdError_Eecdp_Carrier_Edpcch.rst
	Configure_MultiEval_Limit_RcdError_Eecdp_Carrier_Hsdpcch.rst
	Configure_MultiEval_Limit_RcdError_Eecdp_Carrier_Dpdch.rst
	Configure_MultiEval_Limit_RcdError_Eecdp_Carrier_Dpcch.rst