StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:UEPower:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:MODulation:UEPower:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.UePower_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: