Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:HDA:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:HDA:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask_.Hda_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: