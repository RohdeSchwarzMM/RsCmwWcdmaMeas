Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MAXimum
	READ:WCDMa:MEASurement<Instance>:TPC:TOTal:UEPower:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.UePower_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: