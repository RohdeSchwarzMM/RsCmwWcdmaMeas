Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CHIP:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Chip_.Average.Average
	:members:
	:undoc-members:
	:noindex: