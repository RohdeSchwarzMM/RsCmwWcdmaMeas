ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:EOFFset
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:COUNt
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:OSINdex
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:EOFFset
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:COUNt
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:OSINdex
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment.rst
	Configure_MultiEval_ListPy_SingleCmw.rst