Dcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:DCARrier:ABSolute

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EMASk:DCARrier:ABSolute



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.Emask_.Dcarrier.Dcarrier
	:members:
	:undoc-members:
	:noindex: