Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:MERRor:PEAK:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Merror_.Peak_.Average.Average
	:members:
	:undoc-members:
	:noindex: