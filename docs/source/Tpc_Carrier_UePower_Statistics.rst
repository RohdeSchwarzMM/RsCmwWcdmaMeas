Statistics
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:STATistics
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:STATistics

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:STATistics
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:STATistics



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.UePower_.Statistics.Statistics
	:members:
	:undoc-members:
	:noindex: