Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:PERRor:CHIP:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Perror_.Chip_.Current.Current
	:members:
	:undoc-members:
	:noindex: