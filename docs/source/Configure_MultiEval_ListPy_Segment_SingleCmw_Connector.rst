Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.SingleCmw_.Connector.Connector
	:members:
	:undoc-members:
	:noindex: