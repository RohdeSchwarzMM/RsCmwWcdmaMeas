Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:RMS:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:RMS:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:RMS:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:RMS:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.EvMagnitude_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: