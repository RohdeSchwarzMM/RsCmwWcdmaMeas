StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDPower:SDEViation

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CDPower:SDEViation



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.CdPower_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: