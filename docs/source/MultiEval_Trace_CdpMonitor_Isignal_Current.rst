Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:CDPMonitor:ISIGnal:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.CdpMonitor_.Isignal_.Current.Current
	:members:
	:undoc-members:
	:noindex: