Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage
	FETCh:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage
	CALCulate:WCDMa:MEASurement<Instance>:TPC:DHIB:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Dhib_.Average.Average
	:members:
	:undoc-members:
	:noindex: