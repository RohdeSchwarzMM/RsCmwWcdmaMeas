Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:RCDerror:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: