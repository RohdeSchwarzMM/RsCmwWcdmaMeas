MfRight
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfRight.MfRight
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.emask.mfRight.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Emask_MfRight_Average.rst
	MultiEval_Trace_Emask_MfRight_Current.rst
	MultiEval_Trace_Emask_MfRight_Maximum.rst