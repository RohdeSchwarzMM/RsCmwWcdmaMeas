Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:EDPDch<EdpdChannel>:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:EDPDch<EdpdChannel>:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Edpdch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: