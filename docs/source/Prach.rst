Prach
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:MEASurement<Instance>:PRACh
	single: ABORt:WCDMa:MEASurement<Instance>:PRACh
	single: INITiate:WCDMa:MEASurement<Instance>:PRACh

.. code-block:: python

	STOP:WCDMa:MEASurement<Instance>:PRACh
	ABORt:WCDMa:MEASurement<Instance>:PRACh
	INITiate:WCDMa:MEASurement<Instance>:PRACh



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach.Prach
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_State.rst
	Prach_Trace.rst
	Prach_OffPower.rst
	Prach_Preamble.rst