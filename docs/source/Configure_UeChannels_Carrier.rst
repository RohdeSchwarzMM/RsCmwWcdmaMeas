Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.ueChannels.carrier.repcap_carrier_get()
	driver.configure.ueChannels.carrier.repcap_carrier_set(repcap.Carrier.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueChannels.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeChannels_Carrier_Edpdch.rst
	Configure_UeChannels_Carrier_Edpcch.rst
	Configure_UeChannels_Carrier_Hsdpcch.rst
	Configure_UeChannels_Carrier_Dpdch.rst
	Configure_UeChannels_Carrier_Dpcch.rst