OlpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:DELay
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:MGAP
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:SOURce
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:THReshold
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:SLOPe
	single: TRIGger:WCDMa:MEASurement<Instance>:OLPControl:TOUT

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:DELay
	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:MGAP
	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:SOURce
	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:THReshold
	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:SLOPe
	TRIGger:WCDMa:MEASurement<Instance>:OLPControl:TOUT



.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger_.OlpControl.OlpControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.olpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_OlpControl_Catalog.rst