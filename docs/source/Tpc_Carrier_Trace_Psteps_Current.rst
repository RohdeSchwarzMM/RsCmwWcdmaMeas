Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:TRACe:PSTeps:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Trace_.Psteps_.Current.Current
	:members:
	:undoc-members:
	:noindex: