Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:SPECtrum:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Spectrum_.Average.Average
	:members:
	:undoc-members:
	:noindex: