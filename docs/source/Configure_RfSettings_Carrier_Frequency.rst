Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:RFSettings:CARRier<Carrier>:FREQuency

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:RFSettings:CARRier<Carrier>:FREQuency



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.RfSettings_.Carrier_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex: