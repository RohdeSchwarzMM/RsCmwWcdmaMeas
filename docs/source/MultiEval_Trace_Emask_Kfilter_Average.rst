Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:AVERage
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:AVERage

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:AVERage
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.Kfilter_.Average.Average
	:members:
	:undoc-members:
	:noindex: