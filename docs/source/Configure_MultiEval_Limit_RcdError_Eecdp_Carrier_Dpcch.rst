Dpcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:DPCCh

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:RCDerror:EECDp:CARRier<Carrier>:DPCCh



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit_.RcdError_.Eecdp_.Carrier_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex: