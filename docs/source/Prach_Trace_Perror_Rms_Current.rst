Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:RMS:CURRent
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:RMS:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:RMS:CURRent
	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:RMS:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Perror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: