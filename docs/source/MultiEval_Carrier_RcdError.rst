RcdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.RcdError.RcdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.rcdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_RcdError_Current.rst
	MultiEval_Carrier_RcdError_Average.rst
	MultiEval_Carrier_RcdError_Maximum.rst
	MultiEval_Carrier_RcdError_StandardDev.rst
	MultiEval_Carrier_RcdError_Sf.rst
	MultiEval_Carrier_RcdError_OcInfo.rst