Setup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Setup.Setup
	:members:
	:undoc-members:
	:noindex: