Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:TPC:TOTal:TRACe:UEPower:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:TRACe:UEPower:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:TPC:TOTal:TRACe:UEPower:CURRent
	FETCh:WCDMa:MEASurement<Instance>:TPC:TOTal:TRACe:UEPower:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total_.Trace_.UePower_.Current.Current
	:members:
	:undoc-members:
	:noindex: