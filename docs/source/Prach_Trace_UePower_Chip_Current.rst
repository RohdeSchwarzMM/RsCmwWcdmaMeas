Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CHIP:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CHIP:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CHIP:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:UEPower:CHIP:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.UePower_.Chip_.Current.Current
	:members:
	:undoc-members:
	:noindex: