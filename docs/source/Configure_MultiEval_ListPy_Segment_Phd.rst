Phd
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PHD

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PHD



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Phd.Phd
	:members:
	:undoc-members:
	:noindex: