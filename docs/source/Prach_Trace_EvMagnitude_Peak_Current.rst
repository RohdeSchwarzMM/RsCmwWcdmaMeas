Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent
	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.EvMagnitude_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: