Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Average.Average
	:members:
	:undoc-members:
	:noindex: