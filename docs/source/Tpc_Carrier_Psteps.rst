Psteps
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.Psteps.Psteps
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.carrier.psteps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Carrier_Psteps_Maximum.rst
	Tpc_Carrier_Psteps_Minimum.rst
	Tpc_Carrier_Psteps_Average.rst
	Tpc_Carrier_Psteps_Statistics.rst