IlpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:MLENgth
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSEF
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSGH
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSSegment
	single: CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:AEXecution

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:MLENgth
	CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSEF
	CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSGH
	CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:TSSegment
	CONFigure:WCDMa:MEASurement<Instance>:TPC:ILPControl:AEXecution



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Tpc_.IlpControl.IlpControl
	:members:
	:undoc-members:
	:noindex: