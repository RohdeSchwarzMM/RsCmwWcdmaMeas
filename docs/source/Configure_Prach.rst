Prach
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:TOUT
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:MPReamble
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:PPReamble
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:MOEXception

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:TOUT
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:MPReamble
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:PPReamble
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:MOEXception



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach.Prach
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Prach_Limit.rst
	Configure_Prach_Result.rst