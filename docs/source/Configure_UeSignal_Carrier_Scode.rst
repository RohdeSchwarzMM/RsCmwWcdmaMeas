Scode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UESignal:CARRier<Carrier>:SCODe

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UESignal:CARRier<Carrier>:SCODe



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeSignal_.Carrier_.Scode.Scode
	:members:
	:undoc-members:
	:noindex: