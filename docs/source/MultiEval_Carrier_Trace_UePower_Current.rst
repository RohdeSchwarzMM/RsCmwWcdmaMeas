Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:UEPower:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:UEPower:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:UEPower:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:UEPower:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.UePower_.Current.Current
	:members:
	:undoc-members:
	:noindex: