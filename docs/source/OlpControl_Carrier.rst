Carrier<CARRierExt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.olpControl.carrier.repcap_cARRierExt_get()
	driver.olpControl.carrier.repcap_cARRierExt_set(repcap.CARRierExt.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.OlpControl_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.olpControl.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	OlpControl_Carrier_UepPower.rst