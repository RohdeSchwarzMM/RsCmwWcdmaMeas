Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:BC:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:BC:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask_.Bc_.Current.Current
	:members:
	:undoc-members:
	:noindex: