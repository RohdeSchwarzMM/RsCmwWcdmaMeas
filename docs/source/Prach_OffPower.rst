OffPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	single: READ:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	single: CALCulate:WCDMa:MEASurement<Instance>:PRACh:OFFPower

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	READ:WCDMa:MEASurement<Instance>:PRACh:OFFPower
	CALCulate:WCDMa:MEASurement<Instance>:PRACh:OFFPower



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.OffPower.OffPower
	:members:
	:undoc-members:
	:noindex: