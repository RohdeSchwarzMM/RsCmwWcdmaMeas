Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.prach.preamble.repcap_preamble_get()
	driver.prach.preamble.repcap_preamble_set(repcap.Preamble.Nr1)





.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Preamble.Preamble
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.preamble.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Preamble_Current.rst