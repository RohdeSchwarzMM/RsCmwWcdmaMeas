Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum
	single: CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum
	READ:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum
	CALCulate:WCDMa:MEASurement<Instance>:TPC:CARRier<Carrier>:UEPower:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Carrier_.UePower_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: