CdThreshold
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:CDTHreshold:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:CDTHreshold:MODulation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.CdThreshold.CdThreshold
	:members:
	:undoc-members:
	:noindex: