Pcde
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Pcde.Pcde
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.pcde.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Pcde_Code.rst
	MultiEval_ListPy_Pcde_Phase.rst
	MultiEval_ListPy_Pcde_Error.rst
	MultiEval_ListPy_Pcde_Current.rst
	MultiEval_ListPy_Pcde_Maximum.rst