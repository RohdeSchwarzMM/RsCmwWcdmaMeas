Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:PEAK:CURRent
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:PEAK:CURRent

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:PEAK:CURRent
	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:PERRor:PEAK:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.Perror_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: