Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:AB:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:SPECtrum:EMASk:AB:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask_.Ab_.Average.Average
	:members:
	:undoc-members:
	:noindex: