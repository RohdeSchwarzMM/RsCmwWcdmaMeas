Modulation
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Modulation_StandardDev.rst
	MultiEval_Carrier_Modulation_Maximum.rst
	MultiEval_Carrier_Modulation_Current.rst
	MultiEval_Carrier_Modulation_Average.rst