Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:CHIP:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:CHIP:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:CHIP:CURRent
	FETCh:WCDMa:MEASurement<Instance>:PRACh:TRACe:EVMagnitude:CHIP:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.Prach_.Trace_.EvMagnitude_.Chip_.Current.Current
	:members:
	:undoc-members:
	:noindex: