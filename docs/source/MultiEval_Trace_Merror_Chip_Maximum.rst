Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Merror_.Chip_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: