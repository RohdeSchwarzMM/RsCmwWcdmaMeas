Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:MFRight:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfRight_.Current.Current
	:members:
	:undoc-members:
	:noindex: