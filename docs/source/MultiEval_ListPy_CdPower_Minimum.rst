Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:MINimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:MINimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: