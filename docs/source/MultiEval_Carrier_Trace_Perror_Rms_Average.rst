Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:RMS:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:RMS:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:RMS:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:CARRier<Carrier>:TRACe:PERRor:RMS:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.Perror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: