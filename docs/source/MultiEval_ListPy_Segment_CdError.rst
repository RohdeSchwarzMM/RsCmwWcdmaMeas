CdError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Segment_.CdError.CdError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.cdError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_CdError_Current.rst
	MultiEval_ListPy_Segment_CdError_Average.rst
	MultiEval_ListPy_Segment_CdError_Maximum.rst
	MultiEval_ListPy_Segment_CdError_StandardDev.rst