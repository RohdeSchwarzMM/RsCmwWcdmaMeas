UePower
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Carrier_.Trace_.UePower.UePower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.carrier.trace.uePower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Carrier_Trace_UePower_StandardDev.rst
	MultiEval_Carrier_Trace_UePower_Minimum.rst
	MultiEval_Carrier_Trace_UePower_Maximum.rst
	MultiEval_Carrier_Trace_UePower_Average.rst
	MultiEval_Carrier_Trace_UePower_Current.rst