TtError
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Modulation_.TtError.TtError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.ttError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_TtError_Current.rst
	MultiEval_ListPy_Modulation_TtError_Average.rst
	MultiEval_ListPy_Modulation_TtError_Maximum.rst
	MultiEval_ListPy_Modulation_TtError_StandardDev.rst