Dmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:DMODe:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:DMODe:MODulation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Dmode.Dmode
	:members:
	:undoc-members:
	:noindex: