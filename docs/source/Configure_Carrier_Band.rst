Band
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:CARRier<Carrier>:BAND

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:CARRier<Carrier>:BAND



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Carrier_.Band.Band
	:members:
	:undoc-members:
	:noindex: