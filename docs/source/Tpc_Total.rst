Total
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Tpc_.Total.Total
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tpc.total.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tpc_Total_UePower.rst
	Tpc_Total_Trace.rst