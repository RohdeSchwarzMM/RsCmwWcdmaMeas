Emask
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask.Emask
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.emask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Emask_MfLeft.rst
	MultiEval_Trace_Emask_MfRight.rst
	MultiEval_Trace_Emask_HkfLeft.rst
	MultiEval_Trace_Emask_HkfRight.rst
	MultiEval_Trace_Emask_Kfilter.rst