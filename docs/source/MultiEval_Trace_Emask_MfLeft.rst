MfLeft
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.MfLeft.MfLeft
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.emask.mfLeft.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Emask_MfLeft_Average.rst
	MultiEval_Trace_Emask_MfLeft_Current.rst
	MultiEval_Trace_Emask_MfLeft_Maximum.rst