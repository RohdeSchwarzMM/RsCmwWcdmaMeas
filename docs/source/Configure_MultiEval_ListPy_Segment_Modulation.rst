Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex: