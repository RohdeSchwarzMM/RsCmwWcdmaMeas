Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDERror:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Average.Average
	:members:
	:undoc-members:
	:noindex: