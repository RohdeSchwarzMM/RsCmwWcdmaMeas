Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:TXM
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:RCDerror
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:IQ
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:BER
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PSTeps
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PHD
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:FERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:UEPower
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:ALL
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDERror
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDPower
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDPMonitor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:EMASk
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:ACLR
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:MERRor

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:TXM
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:RCDerror
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:IQ
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:BER
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PSTeps
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PHD
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:FERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:UEPower
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:ALL
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDERror
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDPower
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:CDPMonitor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:EMASk
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:ACLR
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:RESult:MERRor



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Result.Result
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Result_Chip.rst