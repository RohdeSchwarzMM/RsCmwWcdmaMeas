Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PHSDpcch
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PHD
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:MERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	single: CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:CFERror

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PHSDpcch
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PHD
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:EVMagnitude
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:MERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:IQOFfset
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:IQIMbalance
	CONFigure:WCDMa:MEASurement<Instance>:MEValuation:LIMit:CFERror



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.MultiEval_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_RcdError.rst
	Configure_MultiEval_Limit_Pcontrol.rst
	Configure_MultiEval_Limit_Emask.rst
	Configure_MultiEval_Limit_Aclr.rst