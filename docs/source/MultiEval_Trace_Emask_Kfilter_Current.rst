Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:CURRent
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:CURRent

.. code-block:: python

	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:CURRent
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:EMASk:KFILter:CURRent



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Emask_.Kfilter_.Current.Current
	:members:
	:undoc-members:
	:noindex: