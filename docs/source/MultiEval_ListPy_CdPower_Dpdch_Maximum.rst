Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:DPDCh:MAXimum

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:LIST:CDPower:DPDCh:MAXimum



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdPower_.Dpdch_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: