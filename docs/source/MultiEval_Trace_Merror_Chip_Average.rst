Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:AVERage
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:AVERage

.. code-block:: python

	FETCh:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:AVERage
	READ:WCDMa:MEASurement<Instance>:MEValuation:TRACe:MERRor:CHIP:AVERage



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Trace_.Merror_.Chip_.Average.Average
	:members:
	:undoc-members:
	:noindex: