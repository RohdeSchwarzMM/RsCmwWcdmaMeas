Chip
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:UEPower
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:PERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:MERRor
	single: CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:EVM

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:UEPower
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:PERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:MERRor
	CONFigure:WCDMa:MEASurement<Instance>:PRACh:RESult:CHIP:EVM



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.Prach_.Result_.Chip.Chip
	:members:
	:undoc-members:
	:noindex: