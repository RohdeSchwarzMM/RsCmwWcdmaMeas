Trigger
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.Trigger.Trigger
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval.rst
	Trigger_Tpc.rst
	Trigger_Prach.rst
	Trigger_OoSync.rst
	Trigger_OlpControl.rst