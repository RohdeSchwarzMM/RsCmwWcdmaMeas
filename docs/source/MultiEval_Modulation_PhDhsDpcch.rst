PhDhsDpcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch
	single: READ:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch
	single: FETCh:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch

.. code-block:: python

	CALCulate:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch
	READ:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch
	FETCh:WCDMa:MEASurement<Instance>:MEValuation:MODulation:PHDHsdpcch



.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Modulation_.PhDhsDpcch.PhDhsDpcch
	:members:
	:undoc-members:
	:noindex: