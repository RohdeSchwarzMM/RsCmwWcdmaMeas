Spectrum
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.Spectrum.Spectrum
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Spectrum_Average.rst
	MultiEval_Spectrum_Current.rst
	MultiEval_Spectrum_Maximum.rst