Dpcch
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.CdError_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.cdError.dpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_CdError_Dpcch_Current.rst
	MultiEval_ListPy_CdError_Dpcch_Average.rst
	MultiEval_ListPy_CdError_Dpcch_Maximum.rst
	MultiEval_ListPy_CdError_Dpcch_StandardDev.rst