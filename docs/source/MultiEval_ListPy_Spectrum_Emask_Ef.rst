Ef
----------------------------------------





.. autoclass:: RsCmwWcdmaMeas.Implementations.MultiEval_.ListPy_.Spectrum_.Emask_.Ef.Ef
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.spectrum.emask.ef.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Spectrum_Emask_Ef_Current.rst
	MultiEval_ListPy_Spectrum_Emask_Ef_Average.rst
	MultiEval_ListPy_Spectrum_Emask_Ef_Maximum.rst