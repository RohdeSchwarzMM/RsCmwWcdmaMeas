Hsdpcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:HSDPcch

.. code-block:: python

	CONFigure:WCDMa:MEASurement<Instance>:UECHannels:CARRier<Carrier>:HSDPcch



.. autoclass:: RsCmwWcdmaMeas.Implementations.Configure_.UeChannels_.Carrier_.Hsdpcch.Hsdpcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueChannels.carrier.hsdpcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeChannels_Carrier_Hsdpcch_Config.rst